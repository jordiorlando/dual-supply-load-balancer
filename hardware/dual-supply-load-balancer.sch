EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Dual Supply Load Balancer"
Date "2019-03-06"
Rev "1.0.0"
Comp "Jordi Pakey-Rodriguez (0xdec.im)"
Comment1 "Copyright 2019 Jordi Pakey-Rodriguez"
Comment2 "Licensed under CERN OHL v1.2 (ohwr.org/cernohl)"
Comment3 ""
Comment4 "J1001"
$EndDescr
Wire Wire Line
	5300 2400 5600 2400
Wire Wire Line
	4300 3300 4300 3600
$Comp
L Device:R_Small R7
U 1 1 5C5AA7CF
P 2700 4300
F 0 "R7" V 2504 4300 50  0000 C CNN
F 1 "47k5" V 2595 4300 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 2700 4300 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_10.pdf" H 2700 4300 50  0001 C CNN
F 4 "Yageo" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RC0603FR-0747K5L" H 0   0   50  0001 C CNN "Manufacturer PN"
	1    2700 4300
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C4
U 1 1 5C5AA8FF
P 2700 4600
F 0 "C4" V 2471 4600 50  0000 C CNN
F 1 "47n" V 2562 4600 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2700 4600 50  0001 C CNN
F 3 "https://api.kemet.com/component-edge/download/datasheet/C0603C473K3RACTU.pdf" H 2700 4600 50  0001 C CNN
F 4 "KEMET" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "C0603C473K3RACTU" H 0   0   50  0001 C CNN "Manufacturer PN"
	1    2700 4600
	0    1    1    0   
$EndComp
Text Notes 700  4400 0    50   ~ 0
R7=(VFR(MAX)-VFR(MIN))/10uA\n=(480mV-25mV)/10uA\n=~~47.5k
Text Notes 2800 1800 0    50   ~ 0
CPO capacitors (C1,C2) not used because fast gate turn-on not needed at 12V\nOtherwise: C1,C2=Ciss*10\n=2.9nF*10\n=29nF\nWhere Ciss is Q1,Q2 input capacitance
Text Notes 700  4700 0    50   ~ 0
C4>=Ciss*10 if fast turn-on is not used\nOtherwise: C4>=Ciss*50\nWhere Ciss is Q1,Q2 input capacitance
Wire Wire Line
	3200 4000 3100 4000
Wire Wire Line
	2500 4000 2500 4300
Wire Wire Line
	2500 4300 2600 4300
Wire Wire Line
	2500 4300 2500 4600
Wire Wire Line
	2500 4600 2600 4600
Connection ~ 2500 4300
Wire Wire Line
	2800 4300 3200 4300
Wire Wire Line
	2800 4600 3200 4600
Wire Wire Line
	3000 6100 3000 6000
Connection ~ 2500 4600
Wire Wire Line
	3900 4800 3900 5100
$Comp
L Device:C_Small C2
U 1 1 5C5AD3BD
P 4000 5100
F 0 "C2" V 3771 5100 50  0000 C CNN
F 1 "DNF" V 3862 5100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4000 5100 50  0001 C CNN
F 3 "~" H 4000 5100 50  0001 C CNN
F 4 "DNF" H 4000 5100 50  0001 C CNN "Fitted"
	1    4000 5100
	0    1    1    0   
$EndComp
Wire Wire Line
	4100 3600 4100 3300
$Comp
L Device:C_Small C1
U 1 1 5C5AD33C
P 4000 3300
F 0 "C1" V 3771 3300 50  0000 C CNN
F 1 "DNF" V 3862 3300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4000 3300 50  0001 C CNN
F 3 "~" H 4000 3300 50  0001 C CNN
F 4 "DNF" H 4000 3300 50  0001 C CNN "Fitted"
	1    4000 3300
	0    1    1    0   
$EndComp
Wire Wire Line
	3900 3600 3900 3300
Wire Wire Line
	3700 3600 3700 3300
Wire Wire Line
	3700 4800 3700 5100
$Comp
L Connector_Generic:Conn_02x01 J1
U 1 1 5C73980A
P 3300 2400
F 0 "J1" H 3350 2617 50  0000 C CNN
F 1 "IN1" H 3350 2526 50  0000 C CNN
F 2 "Connector_Molex:Molex_Mega-Fit_76825-0002_2x01_P5.70mm_Horizontal" H 3300 2400 50  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/768250002_sd.pdf" H 3300 2400 50  0001 C CNN
F 4 "Molex" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "0768250002" H 0   0   50  0001 C CNN "Manufacturer PN"
	1    3300 2400
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x01 J2
U 1 1 5C7398CC
P 3300 6000
F 0 "J2" H 3350 6217 50  0000 C CNN
F 1 "IN2" H 3350 6126 50  0000 C CNN
F 2 "Connector_Molex:Molex_Mega-Fit_76825-0002_2x01_P5.70mm_Horizontal" H 3300 6000 50  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/768250002_sd.pdf" H 3300 6000 50  0001 C CNN
F 4 "Molex" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "0768250002" H 0   0   50  0001 C CNN "Manufacturer PN"
	1    3300 6000
	1    0    0    -1  
$EndComp
Text Label 3800 2400 0    50   ~ 0
VIN1
Text Label 2900 3300 0    50   ~ 0
~EN1
Text Label 3800 6000 0    50   ~ 0
VIN2
Text Label 2900 5100 0    50   ~ 0
~EN2
$Comp
L Device:C_Small C3
U 1 1 5C73ABB2
P 2700 3800
F 0 "C3" V 2471 3800 50  0000 C CNN
F 1 "100n" V 2562 3800 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2700 3800 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/UPY-GPHC_X7R_6.3V-to-50V_18.pdf" H 2700 3800 50  0001 C CNN
F 4 "Yageo" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "CC0603KRX7R9BB104" H 0   0   50  0001 C CNN "Manufacturer PN"
	1    2700 3800
	0    1    1    0   
$EndComp
Wire Wire Line
	3200 3800 3100 3800
Wire Wire Line
	2600 3800 2500 3800
Wire Wire Line
	2500 3800 2500 4000
Connection ~ 2500 4000
Wire Wire Line
	3200 4100 3100 4100
Wire Wire Line
	3100 4100 3100 4000
Connection ~ 3100 4000
Wire Wire Line
	3100 4000 2500 4000
Text Label 5600 2400 2    50   ~ 0
VOUT
Text Label 2900 4300 0    50   ~ 0
RANGE
Text Label 2900 4600 0    50   ~ 0
COMP
Text Label 3900 3300 3    50   ~ 0
CPO1
Text Label 3900 5100 1    50   ~ 0
CPO2
Text Label 4300 3300 3    50   ~ 0
GATE1
Wire Wire Line
	4100 2400 4100 2900
Text Label 4300 5100 1    50   ~ 0
GATE2
Wire Wire Line
	4800 3800 4900 3800
Wire Wire Line
	4900 3800 4900 2400
Wire Wire Line
	4900 2400 4500 2400
Wire Wire Line
	4800 4600 4900 4600
Wire Wire Line
	4900 4600 4900 6000
Wire Wire Line
	4900 6000 4500 6000
Text Label 5100 4000 2    50   ~ 0
FETON1
Text Label 5100 4400 2    50   ~ 0
FETON2
Text Label 4600 2400 0    50   ~ 0
OUT1
Text Label 4600 6000 0    50   ~ 0
OUT2
$Comp
L Device:R_Small R1
U 1 1 5C7887D3
P 5100 2400
F 0 "R1" V 4904 2400 50  0000 C CNN
F 1 "1m" V 4995 2400 50  0000 C CNN
F 2 "Resistor_SMD:R_2512_6332Metric" H 5100 2400 50  0001 C CNN
F 3 "https://www.seielect.com/catalog/sei-csnl.pdf" H 5100 2400 50  0001 C CNN
F 4 "Stackpole" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "CSNL2512FT1L00" H 0   0   50  0001 C CNN "Manufacturer PN"
	1    5100 2400
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R2
U 1 1 5C790664
P 5100 6000
F 0 "R2" V 4904 6000 50  0000 C CNN
F 1 "1m" V 4995 6000 50  0000 C CNN
F 2 "Resistor_SMD:R_2512_6332Metric" H 5100 6000 50  0001 C CNN
F 3 "https://www.seielect.com/catalog/sei-csnl.pdf" H 5100 6000 50  0001 C CNN
F 4 "Stackpole" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "CSNL2512FT1L00" H 0   0   50  0001 C CNN "Manufacturer PN"
	1    5100 6000
	0    1    1    0   
$EndComp
Wire Wire Line
	4900 6000 5000 6000
Connection ~ 4900 6000
Wire Wire Line
	5200 6000 5300 6000
Wire Wire Line
	5300 2400 5200 2400
Wire Wire Line
	5000 2400 4900 2400
Connection ~ 4900 2400
Connection ~ 5300 2400
$Comp
L Device:R_Small R9
U 1 1 5C798FD7
P 2700 5100
F 0 "R9" V 2504 5100 50  0000 C CNN
F 1 "10k" V 2595 5100 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 2700 5100 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_10.pdf" H 2700 5100 50  0001 C CNN
F 4 "Yageo" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RC0603JR-0710KL" H 0   0   50  0001 C CNN "Manufacturer PN"
	1    2700 5100
	0    1    1    0   
$EndComp
Wire Wire Line
	2600 5100 2500 5100
Wire Wire Line
	2500 5100 2500 4600
Wire Wire Line
	3100 2400 3000 2400
Connection ~ 2500 3800
Wire Wire Line
	3100 6000 3000 6000
$Comp
L Device:R_Small R8
U 1 1 5C7A0D62
P 2700 3300
F 0 "R8" V 2504 3300 50  0000 C CNN
F 1 "10k" V 2595 3300 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 2700 3300 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_10.pdf" H 2700 3300 50  0001 C CNN
F 4 "Yageo" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RC0603JR-0710KL" H 0   0   50  0001 C CNN "Manufacturer PN"
	1    2700 3300
	0    1    1    0   
$EndComp
Wire Wire Line
	2600 3300 2500 3300
Wire Wire Line
	2500 3300 2500 3800
$Comp
L Device:R_Small R5
U 1 1 5C7AA59C
P 4300 3200
F 0 "R5" H 4359 3246 50  0000 L CNN
F 1 "0" H 4359 3155 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4300 3200 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_10.pdf" H 4300 3200 50  0001 C CNN
F 4 "Yageo" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RC0603JR-070RL" H 0   0   50  0001 C CNN "Manufacturer PN"
	1    4300 3200
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R6
U 1 1 5C7AA616
P 4300 5200
F 0 "R6" H 4241 5154 50  0000 R CNN
F 1 "0" H 4241 5245 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4300 5200 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_10.pdf" H 4300 5200 50  0001 C CNN
F 4 "Yageo" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RC0603JR-070RL" H 0   0   50  0001 C CNN "Manufacturer PN"
	1    4300 5200
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R3
U 1 1 5C7AD00E
P 4200 2900
F 0 "R3" V 4004 2900 50  0000 C CNN
F 1 "10k" V 4095 2900 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4200 2900 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_10.pdf" H 4200 2900 50  0001 C CNN
F 4 "Yageo" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RC0603JR-0710KL" H 0   0   50  0001 C CNN "Manufacturer PN"
	1    4200 2900
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R4
U 1 1 5C7AD098
P 4200 5500
F 0 "R4" V 4004 5500 50  0000 C CNN
F 1 "10k" V 4095 5500 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4200 5500 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_10.pdf" H 4200 5500 50  0001 C CNN
F 4 "Yageo" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RC0603JR-0710KL" H 0   0   50  0001 C CNN "Manufacturer PN"
	1    4200 5500
	0    1    1    0   
$EndComp
$Comp
L Transistor_FET:CSD16570Q5B Q2
U 1 1 5C7ADB94
P 4300 5900
F 0 "Q2" V 4550 5900 50  0000 C CNN
F 1 "CSD16570Q5B" V 4641 5900 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:TDSON-8-1" H 4500 5825 50  0001 L CIN
F 3 "http://www.ti.com/lit/gpn/csd16570q5b" V 4300 5900 50  0001 L CNN
F 4 "Texas Instruments" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "CSD16570Q5B" H 0   0   50  0001 C CNN "Manufacturer PN"
	1    4300 5900
	0    1    1    0   
$EndComp
Connection ~ 4100 6000
$Comp
L Transistor_FET:CSD16570Q5B Q1
U 1 1 5C7B04F3
P 4300 2500
F 0 "Q1" V 4643 2500 50  0000 C CNN
F 1 "CSD16570Q5B" V 4552 2500 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:TDSON-8-1" H 4500 2425 50  0001 L CIN
F 3 "http://www.ti.com/lit/gpn/csd16570q5b" V 4300 2500 50  0001 L CNN
F 4 "Texas Instruments" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "CSD16570Q5B" H 0   0   50  0001 C CNN "Manufacturer PN"
	1    4300 2500
	0    1    -1   0   
$EndComp
Connection ~ 4100 2400
Wire Wire Line
	4100 5100 4100 5500
Wire Wire Line
	4100 4800 4100 5100
Wire Wire Line
	4300 5100 4300 4800
Wire Wire Line
	4300 5300 4300 5500
Connection ~ 4100 5100
Wire Wire Line
	4100 5500 4100 6000
Connection ~ 4300 5500
Connection ~ 4100 5500
Connection ~ 4100 3300
Connection ~ 4100 2900
Wire Wire Line
	4100 2900 4100 3300
Wire Wire Line
	4300 3100 4300 2900
Wire Wire Line
	4300 2900 4300 2700
Connection ~ 4300 2900
Wire Wire Line
	4300 5500 4300 5700
Wire Wire Line
	2800 3300 3700 3300
Wire Wire Line
	2800 5100 3700 5100
$Comp
L Connector_Generic:Conn_02x02_Top_Bottom J3
U 1 1 5C78D9BD
P 6300 2400
F 0 "J3" H 6350 2617 50  0000 C CNN
F 1 "OUT" H 6350 2526 50  0000 C CNN
F 2 "Connector_Molex:Molex_Mega-Fit_76825-0004_2x02_P5.70mm_Horizontal" H 6300 2400 50  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/768250002_sd.pdf" H 6300 2400 50  0001 C CNN
F 4 "Molex" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "0768250004" H 0   0   50  0001 C CNN "Manufacturer PN"
	1    6300 2400
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6600 2500 6500 2500
Wire Wire Line
	6600 2500 6600 2400
Wire Wire Line
	6600 2400 6500 2400
$Comp
L MCU_Microchip_ATtiny:ATtiny816-M U2
U 1 1 5C79A440
P 8300 4200
F 0 "U2" H 7850 5050 50  0000 C CNN
F 1 "ATtiny816-M" H 8550 5050 50  0000 C CNN
F 2 "Package_DFN_QFN:VQFN-20-1EP_3x3mm_P0.4mm_EP1.7x1.7mm" H 8300 4200 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/40001913A.pdf" H 8300 4200 50  0001 C CNN
F 4 "Microchip" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "ATTINY816-MNR" H 0   0   50  0001 C CNN "Manufacturer PN"
	1    8300 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 4000 4800 4000
Wire Wire Line
	4800 4400 5100 4400
$Comp
L Connector_Generic_MountingPin:Conn_01x04_MountingPin J5
U 1 1 5C7AC66C
P 2500 5700
F 0 "J5" H 2587 5616 50  0000 L CNN
F 1 "PSU2" H 2587 5525 50  0000 L CNN
F 2 "Connector_JST:JST_GH_SM04B-GHS-TB_1x04-1MP_P1.25mm_Horizontal" H 2500 5700 50  0001 C CNN
F 3 "http://www.jst-mfg.com/product/pdf/eng/eGH.pdf" H 2500 5700 50  0001 C CNN
F 4 "JST" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "SM04B-GHS-TB(LF)(SN)-" H 0   0   50  0001 C CNN "Manufacturer PN"
	1    2500 5700
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic_MountingPin:Conn_01x06_MountingPin J6
U 1 1 5C7ACA39
P 6200 5600
F 0 "J6" H 6288 5516 50  0000 L CNN
F 1 "CTRL" H 6288 5425 50  0000 L CNN
F 2 "Connector_JST:JST_GH_SM06B-GHS-TB_1x06-1MP_P1.25mm_Horizontal" H 6200 5600 50  0001 C CNN
F 3 "http://www.jst-mfg.com/product/pdf/eng/eGH.pdf" H 6200 5600 50  0001 C CNN
F 4 "JST" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "SM06B-GHS-TB(LF)(SN)" H 0   0   50  0001 C CNN "Manufacturer PN"
	1    6200 5600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 5C7AD107
P 6000 5400
F 0 "#PWR0103" H 6000 5150 50  0001 C CNN
F 1 "GND" V 6005 5272 50  0000 R CNN
F 2 "" H 6000 5400 50  0001 C CNN
F 3 "" H 6000 5400 50  0001 C CNN
	1    6000 5400
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5C7AD1FC
P 2300 2000
F 0 "#PWR0104" H 2300 1750 50  0001 C CNN
F 1 "GND" V 2305 1872 50  0000 R CNN
F 2 "" H 2300 2000 50  0001 C CNN
F 3 "" H 2300 2000 50  0001 C CNN
	1    2300 2000
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 5C7AD24F
P 2300 5600
F 0 "#PWR0105" H 2300 5350 50  0001 C CNN
F 1 "GND" V 2305 5472 50  0000 R CNN
F 2 "" H 2300 5600 50  0001 C CNN
F 3 "" H 2300 5600 50  0001 C CNN
	1    2300 5600
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR0106
U 1 1 5C7AD307
P 6000 5500
F 0 "#PWR0106" H 6000 5350 50  0001 C CNN
F 1 "+5V" V 6015 5628 50  0000 L CNN
F 2 "" H 6000 5500 50  0001 C CNN
F 3 "" H 6000 5500 50  0001 C CNN
	1    6000 5500
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR0108
U 1 1 5C7AD420
P 2300 5700
F 0 "#PWR0108" H 2300 5550 50  0001 C CNN
F 1 "+5V" V 2315 5828 50  0000 L CNN
F 2 "" H 2300 5700 50  0001 C CNN
F 3 "" H 2300 5700 50  0001 C CNN
	1    2300 5700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8900 3500 9200 3500
Text Label 9200 3500 2    50   ~ 0
UPDI
Wire Wire Line
	8900 3600 9200 3600
Wire Wire Line
	8900 3700 9200 3700
Text Label 9200 3600 2    50   ~ 0
SMBDAT
Text Label 9200 3700 2    50   ~ 0
SMBCLK
Text Label 9200 3800 2    50   ~ 0
LED3
Text Label 9200 3900 2    50   ~ 0
LED1
Text Label 9200 4000 2    50   ~ 0
LED2
Text Label 9200 4100 2    50   ~ 0
PG
$Comp
L Device:R_Small R14
U 1 1 5C7A0368
P 10400 5100
F 0 "R14" V 10204 5100 50  0000 C CNN
F 1 "102k" V 10295 5100 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 10400 5100 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_10.pdf" H 10400 5100 50  0001 C CNN
F 4 "Yageo" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RC0603FR-07102KL" H 0   0   50  0001 C CNN "Manufacturer PN"
	1    10400 5100
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R15
U 1 1 5C7BCC0E
P 10200 5300
F 0 "R15" H 10259 5346 50  0000 L CNN
F 1 "34k" H 10259 5255 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 10200 5300 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_10.pdf" H 10200 5300 50  0001 C CNN
F 4 "Yageo" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RC0603FR-0734KL" H 0   0   50  0001 C CNN "Manufacturer PN"
	1    10200 5300
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0109
U 1 1 5C7BCF60
P 8300 3300
F 0 "#PWR0109" H 8300 3150 50  0001 C CNN
F 1 "+5V" H 8315 3473 50  0000 C CNN
F 2 "" H 8300 3300 50  0001 C CNN
F 3 "" H 8300 3300 50  0001 C CNN
	1    8300 3300
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C5
U 1 1 5C7BD591
P 7400 3500
F 0 "C5" H 7492 3546 50  0000 L CNN
F 1 "100n" H 7492 3455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7400 3500 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/UPY-GPHC_X7R_6.3V-to-50V_18.pdf" H 7400 3500 50  0001 C CNN
F 4 "Yageo" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "CC0603KRX7R9BB104" H 0   0   50  0001 C CNN "Manufacturer PN"
	1    7400 3500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0110
U 1 1 5C7BD6D5
P 7400 3700
F 0 "#PWR0110" H 7400 3450 50  0001 C CNN
F 1 "GND" H 7405 3527 50  0000 C CNN
F 2 "" H 7400 3700 50  0001 C CNN
F 3 "" H 7400 3700 50  0001 C CNN
	1    7400 3700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0111
U 1 1 5C7BD731
P 8300 5400
F 0 "#PWR0111" H 8300 5150 50  0001 C CNN
F 1 "GND" H 8305 5227 50  0000 C CNN
F 2 "" H 8300 5400 50  0001 C CNN
F 3 "" H 8300 5400 50  0001 C CNN
	1    8300 5400
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R18
U 1 1 5C7BEE23
P 9200 5100
F 0 "R18" V 9004 5100 50  0000 C CNN
F 1 "102k" V 9095 5100 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 9200 5100 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_10.pdf" H 9200 5100 50  0001 C CNN
F 4 "Yageo" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RC0603FR-07102KL" H 0   0   50  0001 C CNN "Manufacturer PN"
	1    9200 5100
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R19
U 1 1 5C7BEE29
P 9000 5300
F 0 "R19" H 9059 5346 50  0000 L CNN
F 1 "34k" H 9059 5255 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 9000 5300 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_10.pdf" H 9000 5300 50  0001 C CNN
F 4 "Yageo" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RC0603FR-0734KL" H 0   0   50  0001 C CNN "Manufacturer PN"
	1    9000 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 4900 9000 4900
Wire Wire Line
	9000 4900 9000 5100
Wire Wire Line
	9100 5100 9000 5100
Connection ~ 9000 5100
Wire Wire Line
	9000 5100 9000 5200
$Comp
L Device:R_Small R16
U 1 1 5C7C42E5
P 9800 5100
F 0 "R16" V 9604 5100 50  0000 C CNN
F 1 "102k" V 9695 5100 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 9800 5100 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_10.pdf" H 9800 5100 50  0001 C CNN
F 4 "Yageo" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RC0603FR-07102KL" H 0   0   50  0001 C CNN "Manufacturer PN"
	1    9800 5100
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R17
U 1 1 5C7C42EB
P 9600 5300
F 0 "R17" H 9659 5346 50  0000 L CNN
F 1 "34k" H 9659 5255 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 9600 5300 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_10.pdf" H 9600 5300 50  0001 C CNN
F 4 "Yageo" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RC0603FR-0734KL" H 0   0   50  0001 C CNN "Manufacturer PN"
	1    9600 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	9300 5100 9500 5100
Text Label 9500 5100 2    50   ~ 0
OUT2
Wire Wire Line
	9700 5100 9600 5100
Wire Wire Line
	9600 5200 9600 5100
$Comp
L power:GND #PWR0112
U 1 1 5C7CB568
P 9000 5400
F 0 "#PWR0112" H 9000 5150 50  0001 C CNN
F 1 "GND" H 9005 5227 50  0000 C CNN
F 2 "" H 9000 5400 50  0001 C CNN
F 3 "" H 9000 5400 50  0001 C CNN
	1    9000 5400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0113
U 1 1 5C7CB5C5
P 9600 5400
F 0 "#PWR0113" H 9600 5150 50  0001 C CNN
F 1 "GND" H 9605 5227 50  0000 C CNN
F 2 "" H 9600 5400 50  0001 C CNN
F 3 "" H 9600 5400 50  0001 C CNN
	1    9600 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	9900 5100 10100 5100
Text Label 10100 5100 2    50   ~ 0
OUT1
Wire Wire Line
	8900 4500 9200 4500
Wire Wire Line
	8900 4600 9200 4600
Wire Wire Line
	8900 4700 9200 4700
Text Label 9200 4600 2    50   ~ 0
ON2
Text Label 9200 4700 2    50   ~ 0
PG2
Text Label 9200 4400 2    50   ~ 0
FETON2
Text Label 9200 4500 2    50   ~ 0
~EN2
Wire Wire Line
	7700 4500 7400 4500
Wire Wire Line
	7700 4600 7400 4600
Wire Wire Line
	7700 4700 7400 4700
Text Label 7400 4600 0    50   ~ 0
ON1
Text Label 7400 4700 0    50   ~ 0
PG1
Text Label 7400 4500 0    50   ~ 0
~EN1
Text Label 7400 4400 0    50   ~ 0
FETON1
Wire Wire Line
	10200 5200 10200 5100
Wire Wire Line
	10200 5100 10300 5100
$Comp
L power:GND #PWR0114
U 1 1 5C7DF6D3
P 10200 5400
F 0 "#PWR0114" H 10200 5150 50  0001 C CNN
F 1 "GND" H 10205 5227 50  0000 C CNN
F 2 "" H 10200 5400 50  0001 C CNN
F 3 "" H 10200 5400 50  0001 C CNN
	1    10200 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	10500 5100 10700 5100
Text Label 10700 5100 2    50   ~ 0
VOUT
$Comp
L Device:LED_Small D1
U 1 1 5C8012AF
P 9800 3600
F 0 "D1" V 9846 3532 50  0000 R CNN
F 1 "Red" V 9755 3532 50  0000 R CNN
F 2 "LED_SMD:LED_0603_1608Metric" V 9800 3600 50  0001 C CNN
F 3 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493499/LS%20Q976.pdf" V 9800 3600 50  0001 C CNN
F 4 "OSRAM" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "LS Q976-NR-1" H 0   0   50  0001 C CNN "Manufacturer PN"
	1    9800 3600
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R11
U 1 1 5C803512
P 9800 3400
F 0 "R11" H 9859 3446 50  0000 L CNN
F 1 "150" H 9859 3355 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 9800 3400 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_10.pdf" H 9800 3400 50  0001 C CNN
F 4 "Yageo" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RC0603JR-07150RL" H 0   0   50  0001 C CNN "Manufacturer PN"
	1    9800 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 3900 9800 3900
Wire Wire Line
	9800 3900 9800 3700
$Comp
L Device:LED_Small D2
U 1 1 5C805859
P 10200 3600
F 0 "D2" V 10246 3532 50  0000 R CNN
F 1 "Red" V 10155 3532 50  0000 R CNN
F 2 "LED_SMD:LED_0603_1608Metric" V 10200 3600 50  0001 C CNN
F 3 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493499/LS%20Q976.pdf" V 10200 3600 50  0001 C CNN
F 4 "OSRAM" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "LS Q976-NR-1" H 0   0   50  0001 C CNN "Manufacturer PN"
	1    10200 3600
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R12
U 1 1 5C80585F
P 10200 3400
F 0 "R12" H 10259 3446 50  0000 L CNN
F 1 "150" H 10259 3355 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 10200 3400 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_10.pdf" H 10200 3400 50  0001 C CNN
F 4 "Yageo" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RC0603JR-07150RL" H 0   0   50  0001 C CNN "Manufacturer PN"
	1    10200 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 4000 10200 4000
Wire Wire Line
	10200 4000 10200 3700
$Comp
L power:+5V #PWR0115
U 1 1 5C809B09
P 9800 3300
F 0 "#PWR0115" H 9800 3150 50  0001 C CNN
F 1 "+5V" H 9815 3473 50  0000 C CNN
F 2 "" H 9800 3300 50  0001 C CNN
F 3 "" H 9800 3300 50  0001 C CNN
	1    9800 3300
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0116
U 1 1 5C809B66
P 10200 3300
F 0 "#PWR0116" H 10200 3150 50  0001 C CNN
F 1 "+5V" H 10215 3473 50  0000 C CNN
F 2 "" H 10200 3300 50  0001 C CNN
F 3 "" H 10200 3300 50  0001 C CNN
	1    10200 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 5600 5700 5600
Wire Wire Line
	6000 5700 5700 5700
Wire Wire Line
	6000 5800 5700 5800
Wire Wire Line
	6000 5900 5700 5900
Wire Wire Line
	2300 2200 2000 2200
Wire Wire Line
	2300 2300 2000 2300
Wire Wire Line
	2300 5800 2000 5800
Wire Wire Line
	2300 5900 2000 5900
Text Label 5700 5900 0    50   ~ 0
UPDI
Text Label 5700 5800 0    50   ~ 0
SMBDAT
Text Label 5700 5700 0    50   ~ 0
SMBCLK
Text Label 5700 5600 0    50   ~ 0
PG
Text Label 2000 2300 0    50   ~ 0
ON1
Text Label 2000 2200 0    50   ~ 0
PG1
Text Label 2000 5900 0    50   ~ 0
ON2
Text Label 2000 5800 0    50   ~ 0
PG2
Wire Wire Line
	8300 5400 8300 5100
Wire Wire Line
	10200 5100 10200 4200
Wire Wire Line
	10200 4200 8900 4200
Connection ~ 10200 5100
Wire Wire Line
	9600 5100 9600 4800
Wire Wire Line
	9600 4800 8900 4800
Connection ~ 9600 5100
$Comp
L Device:R_Small R13
U 1 1 5C86E906
P 9400 3400
F 0 "R13" H 9459 3446 50  0000 L CNN
F 1 "150" H 9459 3355 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 9400 3400 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_10.pdf" H 9400 3400 50  0001 C CNN
F 4 "Yageo" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RC0603JR-07150RL" H 0   0   50  0001 C CNN "Manufacturer PN"
	1    9400 3400
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0117
U 1 1 5C87E2A7
P 9400 3300
F 0 "#PWR0117" H 9400 3150 50  0001 C CNN
F 1 "+5V" H 9415 3473 50  0000 C CNN
F 2 "" H 9400 3300 50  0001 C CNN
F 3 "" H 9400 3300 50  0001 C CNN
	1    9400 3300
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0119
U 1 1 5C8B58FE
P 7400 3300
F 0 "#PWR0119" H 7400 3150 50  0001 C CNN
F 1 "+5V" H 7415 3473 50  0000 C CNN
F 2 "" H 7400 3300 50  0001 C CNN
F 3 "" H 7400 3300 50  0001 C CNN
	1    7400 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 2400 3700 2400
Wire Wire Line
	3600 6000 3700 6000
$Comp
L Regulator_Linear:AP2204K-5.0 U3
U 1 1 5C8C9B3E
P 6200 3400
F 0 "U3" H 6200 3742 50  0000 C CNN
F 1 "AP2204K-5.0" H 6200 3651 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 6200 3725 50  0001 C CNN
F 3 "https://www.diodes.com/assets/Datasheets/AP2204.pdf" H 6200 3500 50  0001 C CNN
F 4 "Diodes" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "AP2204K-5.0TRG1" H 0   0   50  0001 C CNN "Manufacturer PN"
	1    6200 3400
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic_MountingPin:Conn_01x04_MountingPin J4
U 1 1 5C7AC568
P 2500 2100
F 0 "J4" H 2587 2016 50  0000 L CNN
F 1 "PSU1" H 2587 1925 50  0000 L CNN
F 2 "Connector_JST:JST_GH_SM04B-GHS-TB_1x04-1MP_P1.25mm_Horizontal" H 2500 2100 50  0001 C CNN
F 3 "http://www.jst-mfg.com/product/pdf/eng/eGH.pdf" H 2500 2100 50  0001 C CNN
F 4 "JST" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "SM04B-GHS-TB(LF)(SN)-" H 0   0   50  0001 C CNN "Manufacturer PN"
	1    2500 2100
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C6
U 1 1 5C8D647E
P 5600 3500
F 0 "C6" H 5692 3546 50  0000 L CNN
F 1 "1u" H 5692 3455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5600 3500 50  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Samsung%20PDFs/CL_Series_MLCC_ds.pdf" H 5600 3500 50  0001 C CNN
F 4 "Samsung" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "CL21B105KBFNNNE" H 0   0   50  0001 C CNN "Manufacturer PN"
	1    5600 3500
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C7
U 1 1 5C8D65AE
P 6600 3500
F 0 "C7" H 6692 3546 50  0000 L CNN
F 1 "2u2" H 6692 3455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6600 3500 50  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Samsung%20PDFs/CL_Series_MLCC_ds.pdf" H 6600 3500 50  0001 C CNN
F 4 "Samsung" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "CL21B225KAFNFNE" H 0   0   50  0001 C CNN "Manufacturer PN"
	1    6600 3500
	1    0    0    -1  
$EndComp
Connection ~ 5300 3300
Wire Wire Line
	5600 3600 5600 3700
$Comp
L power:GND #PWR0120
U 1 1 5C8F3786
P 6200 3700
F 0 "#PWR0120" H 6200 3450 50  0001 C CNN
F 1 "GND" H 6205 3527 50  0000 C CNN
F 2 "" H 6200 3700 50  0001 C CNN
F 3 "" H 6200 3700 50  0001 C CNN
	1    6200 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 3300 6500 3300
Connection ~ 5900 3300
Wire Wire Line
	5900 3400 5900 3300
$Comp
L power:GND #PWR0121
U 1 1 5C905B1F
P 5600 3700
F 0 "#PWR0121" H 5600 3450 50  0001 C CNN
F 1 "GND" H 5605 3527 50  0000 C CNN
F 2 "" H 5600 3700 50  0001 C CNN
F 3 "" H 5600 3700 50  0001 C CNN
	1    5600 3700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0122
U 1 1 5C905B80
P 6600 3700
F 0 "#PWR0122" H 6600 3450 50  0001 C CNN
F 1 "GND" H 6605 3527 50  0000 C CNN
F 2 "" H 6600 3700 50  0001 C CNN
F 3 "" H 6600 3700 50  0001 C CNN
	1    6600 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 3300 5900 3300
Wire Wire Line
	5600 3300 5300 3300
Connection ~ 5600 3300
Wire Wire Line
	5600 3300 5600 3400
$Comp
L power:+5V #PWR0123
U 1 1 5C905FEE
P 6600 3300
F 0 "#PWR0123" H 6600 3150 50  0001 C CNN
F 1 "+5V" H 6615 3473 50  0000 C CNN
F 2 "" H 6600 3300 50  0001 C CNN
F 3 "" H 6600 3300 50  0001 C CNN
	1    6600 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 3700 6600 3600
Wire Wire Line
	6600 3400 6600 3300
Connection ~ 6600 3300
Wire Wire Line
	7400 3300 7400 3400
Wire Wire Line
	7400 3600 7400 3700
$Comp
L power:+5V #PWR0126
U 1 1 5C94E8BC
P 3100 3700
F 0 "#PWR0126" H 3100 3550 50  0001 C CNN
F 1 "+5V" H 3115 3873 50  0000 C CNN
F 2 "" H 3100 3700 50  0001 C CNN
F 3 "" H 3100 3700 50  0001 C CNN
	1    3100 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 3800 3100 3700
Wire Wire Line
	3100 3800 2800 3800
Connection ~ 3100 3800
Wire Wire Line
	8900 4400 9200 4400
$Comp
L Device:R_Small R10
U 1 1 5C943375
P 2000 2400
F 0 "R10" H 2059 2446 50  0000 L CNN
F 1 "10k" H 2059 2355 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 2000 2400 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_10.pdf" H 2000 2400 50  0001 C CNN
F 4 "Yageo" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RC0603JR-0710KL" H 0   0   50  0001 C CNN "Manufacturer PN"
	1    2000 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	7700 4400 7400 4400
$Comp
L power:GND #PWR0125
U 1 1 5C946AEA
P 2000 6100
F 0 "#PWR0125" H 2000 5850 50  0001 C CNN
F 1 "GND" H 2005 5927 50  0000 C CNN
F 2 "" H 2000 6100 50  0001 C CNN
F 3 "" H 2000 6100 50  0001 C CNN
	1    2000 6100
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R20
U 1 1 5C946AE4
P 2000 6000
F 0 "R20" H 2059 6046 50  0000 L CNN
F 1 "10k" H 2059 5955 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 2000 6000 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_10.pdf" H 2000 6000 50  0001 C CNN
F 4 "Yageo" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RC0603JR-0710KL" H 0   0   50  0001 C CNN "Manufacturer PN"
	1    2000 6000
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0107
U 1 1 5C7AD3CD
P 2300 2100
F 0 "#PWR0107" H 2300 1950 50  0001 C CNN
F 1 "+5V" V 2315 2228 50  0000 L CNN
F 2 "" H 2300 2100 50  0001 C CNN
F 3 "" H 2300 2100 50  0001 C CNN
	1    2300 2100
	0    -1   -1   0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 5C96F88F
P 1100 7350
F 0 "H1" H 1200 7401 50  0000 L CNN
F 1 "M3" H 1200 7310 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO7380_Pad" H 1100 7350 50  0001 C CNN
F 3 "~" H 1100 7350 50  0001 C CNN
	1    1100 7350
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 5C973D6F
P 1400 7350
F 0 "H2" H 1500 7401 50  0000 L CNN
F 1 "M3" H 1500 7310 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO7380_Pad" H 1400 7350 50  0001 C CNN
F 3 "~" H 1400 7350 50  0001 C CNN
	1    1400 7350
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H3
U 1 1 5C973E5F
P 1700 7350
F 0 "H3" H 1800 7401 50  0000 L CNN
F 1 "M3" H 1800 7310 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO7380_Pad" H 1700 7350 50  0001 C CNN
F 3 "~" H 1700 7350 50  0001 C CNN
	1    1700 7350
	1    0    0    -1  
$EndComp
NoConn ~ 1100 7450
NoConn ~ 1400 7450
NoConn ~ 1700 7450
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5C97F7CB
P 5300 2300
F 0 "#FLG0102" H 5300 2375 50  0001 C CNN
F 1 "PWR_FLAG" H 5300 2474 50  0000 C CNN
F 2 "" H 5300 2300 50  0001 C CNN
F 3 "~" H 5300 2300 50  0001 C CNN
	1    5300 2300
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5C980E59
P 3000 2300
F 0 "#FLG0101" H 3000 2375 50  0001 C CNN
F 1 "PWR_FLAG" H 3000 2474 50  0000 C CNN
F 2 "" H 3000 2300 50  0001 C CNN
F 3 "~" H 3000 2300 50  0001 C CNN
	1    3000 2300
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0103
U 1 1 5C9813D3
P 3700 2300
F 0 "#FLG0103" H 3700 2375 50  0001 C CNN
F 1 "PWR_FLAG" H 3700 2474 50  0000 C CNN
F 2 "" H 3700 2300 50  0001 C CNN
F 3 "~" H 3700 2300 50  0001 C CNN
	1    3700 2300
	1    0    0    -1  
$EndComp
Connection ~ 3700 2400
Wire Wire Line
	3700 2400 4100 2400
$Comp
L power:PWR_FLAG #FLG0104
U 1 1 5C981438
P 3700 5900
F 0 "#FLG0104" H 3700 5975 50  0001 C CNN
F 1 "PWR_FLAG" H 3700 6074 50  0000 C CNN
F 2 "" H 3700 5900 50  0001 C CNN
F 3 "~" H 3700 5900 50  0001 C CNN
	1    3700 5900
	1    0    0    -1  
$EndComp
Connection ~ 3700 6000
Wire Wire Line
	3700 6000 4100 6000
Wire Wire Line
	3700 2300 3700 2400
Wire Wire Line
	3000 2300 3000 2400
Wire Wire Line
	3700 5900 3700 6000
$Comp
L power:GND #PWR0127
U 1 1 5C994E8F
P 6200 6100
F 0 "#PWR0127" H 6200 5850 50  0001 C CNN
F 1 "GND" H 6205 5927 50  0000 C CNN
F 2 "" H 6200 6100 50  0001 C CNN
F 3 "" H 6200 6100 50  0001 C CNN
	1    6200 6100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0128
U 1 1 5C9950B9
P 2500 6100
F 0 "#PWR0128" H 2500 5850 50  0001 C CNN
F 1 "GND" H 2505 5927 50  0000 C CNN
F 2 "" H 2500 6100 50  0001 C CNN
F 3 "" H 2500 6100 50  0001 C CNN
	1    2500 6100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0129
U 1 1 5C9953B0
P 2500 2500
F 0 "#PWR0129" H 2500 2250 50  0001 C CNN
F 1 "GND" H 2505 2327 50  0000 C CNN
F 2 "" H 2500 2500 50  0001 C CNN
F 3 "" H 2500 2500 50  0001 C CNN
	1    2500 2500
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR0101
U 1 1 5C995E5C
P 6600 2500
F 0 "#PWR0101" H 6600 2300 50  0001 C CNN
F 1 "GNDPWR" H 6604 2346 50  0000 C CNN
F 2 "" H 6600 2450 50  0001 C CNN
F 3 "" H 6600 2450 50  0001 C CNN
	1    6600 2500
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR0102
U 1 1 5C995F20
P 3000 6100
F 0 "#PWR0102" H 3000 5900 50  0001 C CNN
F 1 "GNDPWR" H 3004 5946 50  0000 C CNN
F 2 "" H 3000 6050 50  0001 C CNN
F 3 "" H 3000 6050 50  0001 C CNN
	1    3000 6100
	1    0    0    -1  
$EndComp
$Comp
L Device:Net-Tie_2 NT1
U 1 1 5C9964F1
P 6800 2400
F 0 "NT1" H 6800 2578 50  0000 C CNN
F 1 "Net-Tie" H 6800 2487 50  0000 C CNN
F 2 "NetTie:NetTie-2_SMD_Pad0.5mm" H 6800 2400 50  0001 C CNN
F 3 "~" H 6800 2400 50  0001 C CNN
	1    6800 2400
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0105
U 1 1 5C9A84A5
P 2500 3200
F 0 "#FLG0105" H 2500 3275 50  0001 C CNN
F 1 "PWR_FLAG" H 2500 3374 50  0000 C CNN
F 2 "" H 2500 3200 50  0001 C CNN
F 3 "~" H 2500 3200 50  0001 C CNN
	1    2500 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 3300 5300 6000
$Comp
L power:GND #PWR0130
U 1 1 5C9B5233
P 7000 2500
F 0 "#PWR0130" H 7000 2250 50  0001 C CNN
F 1 "GND" H 7005 2327 50  0000 C CNN
F 2 "" H 7000 2500 50  0001 C CNN
F 3 "" H 7000 2500 50  0001 C CNN
	1    7000 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 2400 7000 2400
Wire Wire Line
	7000 2400 7000 2500
Wire Wire Line
	6700 2400 6600 2400
Connection ~ 6600 2400
$Comp
L power:GNDPWR #PWR0124
U 1 1 5C9BF988
P 3000 2500
F 0 "#PWR0124" H 3000 2300 50  0001 C CNN
F 1 "GNDPWR" H 3004 2346 50  0000 C CNN
F 2 "" H 3000 2450 50  0001 C CNN
F 3 "" H 3000 2450 50  0001 C CNN
	1    3000 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 2500 3000 2400
Connection ~ 3000 2400
$Comp
L power:GND #PWR0131
U 1 1 5C9C8100
P 2500 5200
F 0 "#PWR0131" H 2500 4950 50  0001 C CNN
F 1 "GND" H 2505 5027 50  0000 C CNN
F 2 "" H 2500 5200 50  0001 C CNN
F 3 "" H 2500 5200 50  0001 C CNN
	1    2500 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 5200 2500 5100
Connection ~ 2500 5100
$Comp
L power:GND #PWR0132
U 1 1 5C9F04B0
P 2000 2500
F 0 "#PWR0132" H 2000 2250 50  0001 C CNN
F 1 "GND" H 2005 2327 50  0000 C CNN
F 2 "" H 2000 2500 50  0001 C CNN
F 3 "" H 2000 2500 50  0001 C CNN
	1    2000 2500
	1    0    0    -1  
$EndComp
Connection ~ 6600 2500
Wire Wire Line
	8900 3800 9400 3800
Wire Wire Line
	8900 4100 9200 4100
$Comp
L Device:LED_Small D3
U 1 1 5C7C8CD1
P 9400 3600
F 0 "D3" V 9446 3532 50  0000 R CNN
F 1 "White" V 9355 3532 50  0000 R CNN
F 2 "LED_SMD:LED_0603_1608Metric" V 9400 3600 50  0001 C CNN
F 3 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493068/LW%20Q38E.pdf" V 9400 3600 50  0001 C CNN
F 4 "OSRAM" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "LW Q38E-Q1OO-3K6L-1" H 0   0   50  0001 C CNN "Manufacturer PN"
	1    9400 3600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9400 3700 9400 3800
Wire Wire Line
	5300 2400 5300 3300
Wire Wire Line
	6000 2500 5900 2500
Wire Wire Line
	5900 2500 5900 2400
Connection ~ 5900 2400
Wire Wire Line
	5900 2400 6000 2400
Wire Wire Line
	5600 2500 5600 2400
Connection ~ 5600 2400
Wire Wire Line
	5600 2400 5900 2400
Wire Wire Line
	5600 2700 5600 2800
$Comp
L Device:CP_Small C8
U 1 1 5C7F8629
P 5600 2600
F 0 "C8" H 5688 2646 50  0000 L CNN
F 1 "100u" H 5688 2555 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-7343-43_Kemet-X" H 5600 2600 50  0001 C CNN
F 3 "https://api.kemet.com/component-edge/download/datasheet/T495X107K025ATE150.pdf" H 5600 2600 50  0001 C CNN
F 4 "KEMET" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "T495X107K025ATE150" H 0   0   50  0001 C CNN "Manufacturer PN"
	1    5600 2600
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR0118
U 1 1 5C7F928F
P 5600 2800
F 0 "#PWR0118" H 5600 2600 50  0001 C CNN
F 1 "GNDPWR" H 5604 2646 50  0000 C CNN
F 2 "" H 5600 2750 50  0001 C CNN
F 3 "" H 5600 2750 50  0001 C CNN
	1    5600 2800
	1    0    0    -1  
$EndComp
$Comp
L Graphic:Logo_Open_Hardware_Small #LOGO1
U 1 1 5C7FA65C
P 10900 6950
F 0 "#LOGO1" H 10900 7225 50  0001 C CNN
F 1 "Logo_Open_Hardware_Small" H 10900 6725 50  0001 C CNN
F 2 "" H 10900 6950 50  0001 C CNN
F 3 "~" H 10900 6950 50  0001 C CNN
	1    10900 6950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 2300 5300 2400
Connection ~ 2500 3300
Wire Wire Line
	2500 3200 2500 3300
$Comp
L 0xdec:LTC4370 U1
U 1 1 5C5AAB5E
P 4000 4200
AR Path="/5C5AAB5E" Ref="U1"  Part="1" 
AR Path="/5C5AA9A2/5C5AAB5E" Ref="U?"  Part="1" 
F 0 "U1" H 3350 4750 50  0000 C CNN
F 1 "LTC4370" H 4550 4750 50  0000 C CNN
F 2 "Package_DFN_QFN:DFN-16-1EP_3x4mm_P0.45mm_EP1.7x3.3mm" H 4000 4200 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/4370f.pdf" H 4000 4200 50  0001 C CNN
F 4 "Linear/Analog" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "LTC4370CDE#PBF" H 0   0   50  0001 C CNN "Manufacturer PN"
	1    4000 4200
	1    0    0    -1  
$EndComp
$EndSCHEMATC
