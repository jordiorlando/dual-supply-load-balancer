# Hardware
Hardware is licensed under [CERN OHL v1.2](https://ohwr.org/cernohl). The terms of the license can be found in `CERN_OHL_v1_2.pdf`.

# Firmware
Firmware is licensed under TBD.

# Software
Software is licensed under TBD.
